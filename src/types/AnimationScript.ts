export interface AnimationScript {
  start: number;
  end: number;
  func: () => void;
}