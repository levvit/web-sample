import * as THREE from "three";
import {
  GLTF,
  GLTFLoader
} from 'three/examples/jsm/loaders/GLTFLoader.js';
import {
  OrbitControls
} from 'three/examples/jsm/controls/OrbitControls.js';
import ScrollHandler from './ScrollHandler';
import {
  AnimationScript
} from './types/AnimationScript';
import Levvit from './objects/Levvit'
import {
  AmbientLight,
  CylinderGeometry,
  DirectionalLight,
  Group,
  HemisphereLight,
  MeshStandardMaterial,
  SpotLight,
  sRGBEncoding,
  Vector3
} from "three";
import gsap from "gsap";
import {
  ScrollTrigger
} from 'gsap/ScrollTrigger';
import {
  MultiGeometry
} from './objects/MultiGeometry'

import './styles/index.css'
import './gltfs/Levvit_0715.glb';

class Sample {
  private scene: THREE.Scene;
  private camera: THREE.PerspectiveCamera;
  private renderer: THREE.WebGLRenderer;

  private scrollHandler: ScrollHandler;
  clock: THREE.Clock;
  scrollAnimations: AnimationScript[];
  levvitAnimationMixer: THREE.AnimationMixer;
  levvitJump: THREE.AnimationAction;

  private levvit: Levvit;

  directionalLight: THREE.DirectionalLight;
  spotLight: THREE.SpotLight;

  private levvitOnCylinder: Group;
  private backgroundMeshGroup: Group;

  private flag: boolean;

  private backgroundGeometries: MultiGeometry;

  constructor() {
    this.scene = new THREE.Scene();
    this.setUpCamera();
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
    });
    this.renderer.shadowMap.enabled = true;
    this.renderer.setClearColor(0x000000, 0);
    this.renderer.outputEncoding = sRGBEncoding;

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(this.renderer.domElement);
    window.onresize = this.windowResize.bind(this);

    gsap.registerPlugin(ScrollTrigger);

    this.clock = new THREE.Clock();

    this.backgroundGeometries = new MultiGeometry();

    this.scrollHandler = new ScrollHandler('scrollProgress');
    this.flag = false;

    // const axesHelper = new THREE.AxesHelper(20);
    // this.scene.add( axesHelper );

    // this.turnOnOrbitControls();
    this.loadBackgroundMesh();
    this.setUpLight();
    this.loadGLTF();
    // this.setUpAnimationScript();
    this.animate();
  }

  turnOnOrbitControls = () => {
    new OrbitControls(this.camera, this.renderer.domElement);
  }

  setUpCamera = () => {
    this.camera = new THREE.PerspectiveCamera(65, window.innerWidth / window.innerHeight, 0.1, 10);
    this.camera.position.y = 0.5;
    this.camera.position.z = 3;
  }

  turnOnGridHelper = (size: number, divisions: number) => {
    const gridHelper = new THREE.GridHelper(size, divisions, 0xaec6cf, 0xaec6cf);
    this.scene.add(gridHelper);
  }

  windowResize = () => {
    window.addEventListener('resize', () => {
      this.renderer.setSize(window.innerWidth, window.innerHeight);
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
    })
  }

  loadBackgroundMesh = () => {
    this.backgroundMeshGroup = new THREE.Group();
    this.scene.add(this.backgroundMeshGroup);

    this.scene.add(this.backgroundGeometries.group);
    this.backgroundGeometries.addBox(new Vector3(-3, 1.5, -2), new Vector3(0, 0, 2), 0x5CD9D3);
    this.backgroundGeometries.addCone(new Vector3(-1.5, 0.5, -0.5), new Vector3(0.6, 1, 0.3), 0xFFD86F);
    this.backgroundGeometries.addTorus(new Vector3(-1, 0.8, -1), new Vector3(-0.5, -0.3, 0), 0xF17502);
    this.backgroundGeometries.addTorus(new Vector3(1, 0.3, -4), new Vector3(-0.9, 0.8, 0), 0x64A8F7);
    this.backgroundGeometries.addTorus(new Vector3(1.5, 0.6, -2), new Vector3(2.2, 0.8, 0), 0x9BF0F0);
    this.backgroundGeometries.addTorus(new Vector3(1.8, 0, -1.2), new Vector3(-1, 0.3, 0), 0xC7B9FF);
    this.backgroundGeometries.addTripleIcosahedron(new Vector3(-2.5, -1.5, -2.5), new Vector3(-0.5, 0, -0.8), 0xFE818E);
    this.backgroundGeometries.addTorusKnot(new Vector3(3, 2, -3), new Vector3(2.5, 0.8, 0), 0xC7B9FF);

    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.exhibition-hall',
        start: 'top top',
        // end: 'bottom 80%',
        markers: true,
        scrub: true,
      },
    })

    tl.fromTo(this.backgroundGeometries.group.position, {
      y: 0,
    }, {
      y: 8
    })
  }

  loadGLTF = () => {
    const loader = new GLTFLoader();
    loader.load('src/gltfs/Levvit_0715.glb', (levvit) => {
        this.levvit = new Levvit(levvit);
        this.levvit.playAnimation('Idle');

        this.levvitOnCylinder = new Group();

        const geometry = new CylinderGeometry(0.8, 0.8, 0.3, 64, 64);
        const material = new MeshStandardMaterial({
          color: 0xF3E9F3,
          roughness: 1,
          metalness: 0.5,
          emissive: 0x585555,
        });
        const cylinder = new THREE.Mesh(geometry, material);
        cylinder.position.y = -0.18;

        this.levvitOnCylinder.add(this.levvit.scene);
        this.levvitOnCylinder.add(cylinder);

        this.scene.add(this.levvitOnCylinder);

        const tl = gsap.timeline({
          scrollTrigger: {
            trigger: '.exhibition-hall',
            start: 'top top',
            // end: 'bottom 80%',
            markers: true,
            scrub: true,
          },
        })

        tl.fromTo(this.levvitOnCylinder.position, {
          y: 0,
        }, {
          y: 8
        })
      },
      (event: ProgressEvent < EventTarget > ) => {
        console.log((event.loaded / event.total * 100) + '% loaded');
      },
      (error: ErrorEvent) => {
        console.error(error)
      }
    );
  }

  setUpLight = () => {
    const light = new AmbientLight(0xffffff, 0.2); // soft white light
    this.scene.add(light);

    const hemisphereLight = new HemisphereLight(0xEEDFF0, 0xBBAABD, 0.6);
    this.scene.add(hemisphereLight);

    // this.spotLight = new SpotLight(0xFAF8F7, 0.5);
    // this.spotLight.position.set(0, 5, 0);
    // this.spotLight.castShadow = true;

    this.spotLight = new SpotLight(0xffffff, 0.5, 50, 0.7, 0.2, 1);
    this.spotLight.position.set(0, 5, 2);

    this.scene.add(this.spotLight);

    // this.directionalLight = new DirectionalLight(0xffffff, 0.1);
    // this.directionalLight.position.set(0, 5, 0); // ~60º
    // this.directionalLight.target.position.set(0, 0, 5);


    this.directionalLight = new DirectionalLight(0xffffff, 0.5);
    this.directionalLight.color.setHSL( 0.1, 1, 0.95 );
    this.directionalLight.position.set( 4, 5, 1);
    this.directionalLight.position.multiplyScalar( 30 );
    this.directionalLight.castShadow = true;
    this.scene.add(this.directionalLight);
  }

  animate = () => {
    requestAnimationFrame(this.animate);
    this.renderer.render(this.scene, this.camera);
    // this.spotLight.position.set(
    //   this.camera.position.x + 10,
    //   this.camera.position.y + 10,
    //   this.camera.position.z + 10
    // )
    const delta = this.clock.getDelta();
    if (this.levvit) {
      this.levvit.timer(delta);
      this.levvit.scene.rotateY(0.03);
      // this.playScrollAnimations();
    }

    if (this.backgroundGeometries) {
      if (!this.flag) {
        this.backgroundGeometries.group.position.y -= 0.0003;
        if (this.backgroundGeometries.group.position.y < -0.02) this.flag = true;
      } else {
        this.backgroundGeometries.group.position.y += 0.0003;
        if (this.backgroundGeometries.group.position.y > 0) this.flag = false;
      }
    }
  }

  // setUpAnimationScript = () => {
  //   this.scrollAnimations = new Array<AnimationScript>();
  //   this.scrollAnimations.push({
  //     start: 0,
  //     end: 50,
  //     func: () => {
  //       this.levvit.scene.position.y = this.scrollHandler.linerInterpolation(0,10,this.scrollHandler.scalePercent(0,100));
  //     }
  //   })
  // }

  // playScrollAnimations =() => {
  //   this.scrollAnimations.forEach((animation) => {
  //     if (this.scrollHandler.percent >= animation.start && this.scrollHandler.percent < animation.end) {
  //       animation.func();
  //     }
  //   })
  // }
}

window.onload = () => {
  window.scrollTo({
    top: 0,
    behavior: 'smooth'
  });
  new Sample();
}