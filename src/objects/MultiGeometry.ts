import {
  BoxGeometry,
  BufferGeometry,
  ConeGeometry,
  CylinderGeometry,
  Group,
  IcosahedronGeometry,
  Mesh,
  MeshStandardMaterial,
  TorusGeometry,
  TorusKnotGeometry,
  Vector3
} from "three";

type Geometry = "Box" | "Cone" | "Cylinder" | "Icosahedron" | "Torus" | "TorusKnot";

type Geometries = {
  [keys in Geometry]: BufferGeometry;
}


export class MultiGeometry {
  private geometries: Geometries;
  private _group: Group;

  constructor() {
    this.geometries = {
      "Box": new BoxGeometry(0.3, 0.3, 0.3, 64, 64, 64),
      "Cone": new ConeGeometry(0.2, 0.5, 64),
      "Cylinder": new CylinderGeometry(0.2, 0.2, 0.2, 64),
      "Icosahedron": new IcosahedronGeometry(0.2, 8),
      "Torus": new TorusGeometry(0.2, 0.07, 64, 32),
      "TorusKnot": new TorusKnotGeometry(0.2, 0.05, 300, 20, 1, 2),
    };
    this._group = new Group();
  }

 /**
  * addBox
  */
 public addBox(position?: Vector3, rotation?: Vector3, color?: number) {
    const material = new MeshStandardMaterial({
      color: color ? color : 0xFFFFFF,
      roughness: 1,
      metalness: 0.1,
      emissive: 0x100f0f,
    });
    const object = new Mesh( this.geometries.Box, material );

    if (!position) position = new Vector3(0,0,0);
    if (!rotation) rotation = new Vector3(0,0,0);

    object.position.set(position.x, position.y, position.z);

    object.rotation.set(rotation.x, rotation.y, rotation.z)

    this._group.add( object );
 }

 public addCone(position?: Vector3, rotation?: Vector3, color?: number) {
    const material = new MeshStandardMaterial({
      color: color ? color : 0xFFFFFF,
      roughness: 1,
      metalness: 0.1,
      emissive: 0x100f0f,
    });
    const object = new Mesh( this.geometries.Cone, material );

    if (!position) position = new Vector3(0,0,0);
    if (!rotation) rotation = new Vector3(0,0,0);

    object.position.set(position.x, position.y, position.z);

    object.rotation.set(rotation.x, rotation.y, rotation.z)

    this._group.add( object );
  }

  /**
   * addTorus
   */
  public addTorus(position?: Vector3, rotation?: Vector3, color?: number) {
    const material = new MeshStandardMaterial({
      color: color ? color : 0xFFFFFF,
      roughness: 1,
      metalness: 0.1,
      emissive: 0x100f0f,
    });
    const object = new Mesh( this.geometries.Torus, material );

    if (!position) position = new Vector3(0,0,0);
    if (!rotation) rotation = new Vector3(0,0,0);

    object.position.set(position.x, position.y, position.z);

    object.rotation.set(rotation.x, rotation.y, rotation.z);

    this._group.add( object );
  }

  /**
   * addTriple
   */
  public addTripleIcosahedron(position?: Vector3, rotation?: Vector3, color?: number) {
    const material = new MeshStandardMaterial({
      color: color ? color : 0xFFFFFF,
      roughness: 1,
      metalness: 0.1,
      emissive: 0x100f0f,
    });

    const tripleIcosahedron = new Group();
    
    for (let i = 0; i < 3; i++) {
      const object = new Mesh( this.geometries.Icosahedron, material );
      object.position.set(0,i*0.35,0);
      tripleIcosahedron.add(object);
    }

    tripleIcosahedron.position.set(position.x, position.y, position.z);

    tripleIcosahedron.rotation.set(rotation.x, rotation.y, rotation.z);

    this._group.add( tripleIcosahedron );
  }

  /**
   * addTorusKnot
   */
  public addTorusKnot(position?: Vector3, rotation?: Vector3, color?: number) {
    const material = new MeshStandardMaterial({
      color: color ? color : 0xFFFFFF,
      roughness: 1,
      metalness: 0.1,
      emissive: 0x100f0f,
    });
    const object = new Mesh( this.geometries.TorusKnot, material );

    if (!position) position = new Vector3(0,0,0);
    if (!rotation) rotation = new Vector3(0,0,0);

    object.position.set(position.x, position.y, position.z);

    object.rotation.set(rotation.x, rotation.y, rotation.z);

    this._group.add( object );
  }

 /**
  * geometryFactory
  */
 public geometryFactory() {
  
 }

 
 public get group() : Group {
  return this._group;
 }
}