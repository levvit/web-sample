import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import * as THREE from "three";
import { AnimationMixer, LoopOnce } from 'three';

type AnimationType = 'Idle' | 'Jump' | 'Jump_Down' | 'Walk' | 'Cylinder001|Take 001|BaseLayer';
type AnimationClip = {
  [key in AnimationType]: THREE.AnimationAction;
}
interface Animation extends AnimationClip {};

type Direction = 'left' | 'right' | 'front' | 'back' | 'others';

export default class {
  private _levvit: GLTF;
  private _mixer: THREE.AnimationMixer;

  private _animation: Animation;

  private _runningAnimation: AnimationType | 'none';

  constructor(_levvit: GLTF) {
    this._levvit = _levvit;
    this._animation = {
      Idle: null,
      Jump: null,
      Jump_Down: null,
      Walk: null,
      "Cylinder001|Take 001|BaseLayer": null,
    }
    this._setUpAnimation();
    this._runningAnimation = 'none';
  }

  private _setUpAnimation = () => {
    this._mixer = new AnimationMixer(this._levvit.scene);

    this._levvit.animations.forEach(animation => {
      const name = animation.name as AnimationType;
      this._animation[name] = this._mixer.clipAction(animation);
      
    });
  }

  /**
   * turnRight
   * @param {number} delta - delta of rotation y.
   */
  public turnRight(delta: number) {
    const r = this._levvit.scene.rotation.y;
    const PI = Math.PI;
    if (r === PI/2)  return;
    if (PI/2 < r && r <= 3*PI/2) {
      this._levvit.scene.rotation.y -= delta;
      if (this._levvit.scene.rotation.y < PI/2) this._levvit.scene.rotation.y = PI/2;
    } else if (3*PI/2 < r && r < 2*PI || 0 <= r && r < PI/2) {
      this._levvit.scene.rotation.y += delta;
      if (this._levvit.scene.rotation.y > 2*PI) this._levvit.scene.rotation.y = 0;
    } else this._levvit.scene.rotation.y = PI/2;
    // const y = this._levvit.scene.rotation.y;
    // if (3/2*Math.PI<= y &&  y < 2*Math.PI) {
    //   this._levvit.scene.rotation.y += delta;
    //   if (this._levvit.scene.rotation.y > 2*Math.PI) {
    //     this._levvit.scene.rotation.y = 0;
    //   }
    // } else if (0 <= y && y < 1/2*Math.PI ) {
    //   this._levvit.scene.rotation.y += delta;
    //   if (this._levvit.scene.rotation.y > 1/2*Math.PI) {
    //     this._levvit.scene.rotation.y = 1/2*Math.PI - 0.0000001;
    //   }
    // } else {
    //   this._levvit.scene.rotation.y = 3/2*Math.PI;
    // }
  }

  /**
   * turnLeft
   * @param {number} delta - delta of rotation y.
   */
  public turnLeft(delta: number) {
    const r = this._levvit.scene.rotation.y;
    const PI = Math.PI;
    if (r === 3*PI/2)  return;

    if (0 <= r && r <= PI/2 || 3*PI/2 < r && r <= 2*PI )  {
      this._levvit.scene.rotation.y -= delta;
      
      if (this._levvit.scene.rotation.y < 0) this._levvit.scene.rotation.y = 2*PI;
    } else if (PI/2 < r && r < 3*PI/2) {
      this._levvit.scene.rotation.y += delta;
      if (this._levvit.scene.rotation.y > 3*PI/2) this._levvit.scene.rotation.y = 3*PI/2;
    } else this._levvit.scene.rotation.y = 3*PI/2;

    // if (0 < r && r <= PI/2 || 3*PI/2 < r && r <= 2*PI) {
    //   this._levvit.scene.rotation.y -= delta;
    //   if (this._levvit.scene.rotation.y < 0) {
    //     this._levvit.scene.rotation.y = 2*PI;
    //   }
    // } else if (PI/2 < r && r < 3*PI/2) {
    //   this._levvit.scene.rotation.y += delta;
    //   if (this._levvit.scene.rotation.y > 3*PI/2) {
    //     this._levvit.scene.rotation.y = 3*PI/2;
    //   }
    // } else this._levvit.scene.rotation.y = 3*PI/2;

    // if (3/2*Math.PI<= y &&  y < 2*Math.PI) {
    //   this._levvit.scene.rotation.y -= delta;
    //   if (this._levvit.scene.rotation.y < 3/2*Math.PI) {
    //     this._levvit.scene.rotation.y = 3/2*Math.PI;
    //   }
    // }else if (0 < y && y <= 1/2*Math.PI ) {
    //   this._levvit.scene.rotation.y -= delta;
    //   if (this._levvit.scene.rotation.y < 0) {
    //     this._levvit.scene.rotation.y = 2*Math.PI - 0.0000001;
    //   }
    // } else {
    //   this._levvit.scene.rotation.y = 1/2*Math.PI;
    // }
  }

  public turnFront(delta: number) {
    const rotation = this._levvit.scene.rotation.y;
    if (rotation === 2*Math.PI || rotation === 0)  return;
    if ( rotation < 2*Math.PI && Math.PI <= rotation ) {
      this._levvit.scene.rotation.y += delta;
    } else if (rotation < Math.PI && 0 < rotation) {
      this._levvit.scene.rotation.y -= delta;
    } else this._levvit.scene.rotation.y = 0;
  }

  public turnBack(delta: number) {
    const rotation = this._levvit.scene.rotation.y;
    if (rotation === Math.PI)  return;
    if ( rotation <= 2*Math.PI && Math.PI < rotation ) {
      this._levvit.scene.rotation.y -= delta;
    } else if (0 <= rotation && rotation < Math.PI ) {
      this._levvit.scene.rotation.y += delta;
    } else {
      console.log('??', rotation);
      this._levvit.scene.rotation.y = Math.PI;
    }
  }

  //** true: right false: left */
  public get direction() : Direction {
    let direction: Direction = 'others';
    switch (this._levvit.scene.rotation.y) {
      case Math.PI*3/2:
        direction = 'left';
        break;
      case Math.PI:
        direction = 'back';
        break;
      case Math.PI/2:
        direction = 'right';
        break;
      case Math.PI*2:
      case 0:
        direction = 'front';
        break;
      default:
        direction = 'others';
        break;
    }
    return direction;
  }

  // public get animations() : THREE.AnimationClip[] {
  //   return this._levvit.animations;
  // }

  public get scene() : THREE.Group {
    return this._levvit.scene;
  }

  public jump = () => {
    this._animation.Jump.play();
  }

  public stop = () => {
    this._animation.Jump.fadeOut(1);
  }

  public get isRunning() : boolean {
    return this._animation.Jump.isRunning();
  }

  public timer = (delta: number) => {
    if (this._mixer) this._mixer.update(delta);
  }

  public playAnimation = (type: AnimationType) => {
    if (this._runningAnimation != type) {
      this._runningAnimation = type;
      // this._mixer.stopAllAction();
      // this._animation[type]?.stop();
      this._animation[type]?.play();
    }
  }

  public stopAnimation = (type: AnimationType) => {
    this._runningAnimation = 'none';
    this._animation[type]?.fadeOut(0.3);
  }

  public pauseAnimation = (type: AnimationType) => {
  }

  public get runningAnimation(): AnimationType | 'none' {
    return this._runningAnimation;
  }
}
